import React from 'react'
import { Nav, Navbar } from 'react-bootstrap'

export const TopBar = () => {

    return (
        <>
            <Navbar collapseOnSelect expand="lg" bg="primary" variant="dark" className="justify-content-between">
                <Navbar.Brand href="#home">React OpenWeatherMap App</Navbar.Brand>
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>Iceyisaak</Navbar.Text>   
                </Navbar.Collapse>
            </Navbar>
        </>
    )
}