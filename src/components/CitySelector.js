import React, { useState } from 'react'
import { Row, Col, FormControl, Button } from 'react-bootstrap'
import '../App.css';

export const CitySelector = (props) => {

    const [city, setCity] = useState(null)
    const { onSelectButtonClick } = props

    return (
        <div className="citySelector">
            <Row>
                <Col>
                    <h1>Please Select Your City</h1>
                </Col>
            </Row>
            <Row>
                <Col xs={4}>
                    <FormControl
                        placeholder="Enter City"
                        onChange={e => setCity(e.target.value)} />
                </Col>
            </Row>
            <Row>
                <Col><Button onClick={() => onSelectButtonClick(city)}>Check Weather</Button></Col>
            </Row>
        </div>
    )

}
