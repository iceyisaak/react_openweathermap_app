import React from 'react'
import { Card } from 'react-bootstrap'

export const WeatherCard = (props) => {

    const { dt, min, max, main, icon } = props
    const date = new Date(dt)

    return (
        <Card border="secondary">
            <Card.Img variant="top" src={`http://openweathermap.org/img/wn/${icon}@2x.png`} />
            <Card.Body>
                <Card.Title>{main}</Card.Title>
                <p>
                    {date.getFullYear()} - {date.getMonth() + 1} - {date.getDate()}
                </p>
                <p>Min: {min}°C</p>
                <p>Max: {max}°C</p>
            </Card.Body>
        </Card>
    )
}
