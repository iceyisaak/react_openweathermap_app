import React from 'react';
import { Container } from 'react-bootstrap'
import './App.css';
import { apiKey, baseUrl } from './config/index'

import { useFetch } from './hooks/useFetch'

import { CitySelector } from './components/CitySelector';
import { WeatherList } from './components/WeatherList';
import {TopBar} from './components/TopBar'


function App() {

  const { data, error, inProgress, setUrl } = useFetch()
  // console.log(data)
  const getContent = () => {
    if (error) return <h2>Error when fetching: {error}</h2>
    if (!data && inProgress) return <h2>Loading...</h2>
    if (!data) return null
    return <WeatherList weathers={data.list} />
  }

  return (
    <>
    <TopBar/>
    <Container className="App">
      <CitySelector onSelectButtonClick={city => setUrl(`${baseUrl}/data/2.5/forecast?APPID=${apiKey}&q=${city}&units=metric&cnt=5`)} className="citySelector"/>
      {getContent()}
    </Container>
    </>
  );
}

export default App;
